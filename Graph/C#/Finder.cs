﻿using System;
using System.Collections;

namespace Graf
{
    public class Finder : IFinder
    {
        public string FromRight(Customers customers, int numberFromRight)
        {
            // YOUR SOLUTION GOES HERE

            
            /*
            Because the performance is the goal, it is the best option to iterate the graph just once. In order 
            to do that and still manage to get n-th place from the right end, the queue can be used. 
            The queue will be the size of the 'number from right'. The elements will fill and leave 
            the queue as the program iterates through the list. When the program is finished iterating, 
            the element at the start of the queue is our searched element. 
            */

            var elementsInQ = 0;

            Queue myQ = new Queue();

            if (numberFromRight < 1)
                return "invalid input";

            while (customers != null)
            {
                if (elementsInQ < numberFromRight)
                {
                    myQ.Enqueue(customers.Person);
                    elementsInQ++;
                    customers = customers.Next;
                }


                if (elementsInQ == numberFromRight && customers != null)
                {
                    myQ.Dequeue();
                    myQ.Enqueue(customers.Person);
                    customers = customers.Next;
                }

            }

            if (numberFromRight > elementsInQ)
            {
               return "there are not enough elements to get that possition";
            }

            return "result: " + elementsInQ.ToString() + " " + myQ.Peek();
        }
    }

}
