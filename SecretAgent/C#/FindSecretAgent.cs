﻿using System;
using System.Collections.Generic;

namespace SecretAgent
{
    public class FindSecretAgent : IFindSecretAgent
    {
        public int StartSearch(IEnumerable<int> ids)
        {
            // YOUR SOLUTION GOES HERE

            /* the goal here is to iterate the soldiers only once
             * to do it and find the agent we can compare sums in ideal and corrupted scenario 
             * the difference here is the key 
             * 
             *  1 2 3 4 5 	15	how should it be, ideal sum is 15
             * 
             *  next are the actual sums with agent with different ids as an example
             *  1 2 3 4 1 	11	difference 4    agent was 1
             *  1 2 3 4 2 	12	difference 3    agent was 2
             *  1 2 3 4 3 	13	difference 2    agent was 3
             *  1 2 3 4 4 	14	difference 1    agent was 4
             * 
             *  we can get the agents id with this formula ->       highest ideal id  -  difference
             * */
            var idealSum = 0;
            var idealSumSoldierID = 1;
            var actualSum = 0;

            foreach (int actualId in ids)
                {
                    idealSum = idealSum + idealSumSoldierID;

                    idealSumSoldierID++;
                    
                    actualSum = actualSum + actualId;
                }

            //Console.WriteLine("ideal sum is " + idealSum.ToString());
            //Console.WriteLine("actual sum is " + actualSum.ToString());

            var difference = idealSum - actualSum;
            
            var agentsId = idealSumSoldierID - difference - 1;              //not to forget -1 here becasue I am incrementing the idealSunSoldierID one extra time

            Console.WriteLine("agents id is : " + agentsId.ToString());

            System.Console.ReadLine();


            return agentsId;
        }
    }
}
