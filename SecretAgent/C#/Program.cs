﻿using System.Collections.Generic;

namespace SecretAgent
{
	class Program
	{
		static void Main(string[] args)
		{
			//var agents = new int[] {2,3,2,1,4}; //creating the group of soldiers including the secret agent 

            var agents = new int[] {1,2,3,6,4,5,6,7,8,9};

            var finder = new FindSecretAgent();

            finder.StartSearch(agents);

            // the array data type implements IEnumerable<T> therefore we can use it

		}
	}
}
