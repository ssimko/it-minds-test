﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace StolenTests
{
    [TestClass]
    public class NumberHelperTests
    {
        /*
         * This section contains a very basic test example
         * The test in this document should be re-written, as it does not represent best practices
         */
        
        /*
        [TestMethod]
        public void TestExample()
        {
            // Arrange
            var haystackExample = new List<int> { 1, 2, 3 };

            const int needle = 1;
            var helper = new NumberHelper();

            // Act
            var results = helper.FindClosestNumbers(needle, haystackExample, 1).ToList();

            // Assert
            Assert.Equals(results.ElementAt(0), 1);
        }
        */

        /* here we can use the examples from the decription because as they describe the concept, they can 
         * be used as a good testing cases as well + other edge cases
         * */


        [TestMethod]
        public void FindClosestNumbersTest_OneClosest()
        {
            //Arrange
            var helper = new NumberHelper();
                
            var needle = 1;
            var haystack = new int[] {1,2,3};
            var nClosest = 1;
            
            //Act
            var result = helper.FindClosestNumbers(needle, haystack, nClosest).ToList();

            //Assert
            Assert.Equals(result.ElementAt(0), 1);
        }
        
        [TestMethod]
        public void FindClosestNumbersTest_MultipleClosest()
        {
            //Arrange
            var helper = new NumberHelper();
                
            var needle = 11;
            var haystack = new int[] {2,3,10};
            var nClosest = 2;
            
            //Act
            var result = helper.FindClosestNumbers(needle, haystack, nClosest).ToArray();

            //Assert
            Assert.Equals(result, new int[]{10,3});
        }

        [TestMethod]
        public void FindClosestNumbersTest_EquallyClose_GetSmallerNumber()
        {
            //Arrange
            var helper = new NumberHelper();
                
            var needle = 4;
            var haystack = new int[] {3,5};
            var nClosest = 1;
            
            //Act
            var result = helper.FindClosestNumbers(needle, haystack, nClosest).ToList();

            //Assert
            Assert.Equals(result.ElementAt(0), 3);
        }

        [TestMethod]
        public void FindClosestNumbersTest_UnsortedHaystack()
        {
            //Arrange
            var helper = new NumberHelper();
                
            var needle = 4;
            var haystack = new int[] {10,1,3,9,8};
            var nClosest = 1;
            
            //Act
            var result = helper.FindClosestNumbers(needle, haystack, nClosest).ToList();

            //Assert
            Assert.Equals(result.ElementAt(0), 3);
        }

        [TestMethod]
        public void FindClosestNumbersTest_EmptyHaysatck()
        {
            //Arrange
            var helper = new NumberHelper();
                
            var needle = 4;
            var haystack = new int[] {};
            var nClosest = 1;
            
            //Act and assert 
            try 
            { 
                var result = helper.FindClosestNumbers(needle, haystack, nClosest).ToList();
                Assert.Fail("no exception thrown");
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is OurEmptyHaystackException); //define this exception 
            }
        }

        [TestMethod]
        public void FindClosestNumbersTest_TheSameNumberAsInHaystack()
        {
            //Arrange
            var helper = new NumberHelper();
                
            var needle = 4;
            var haystack = new int[] {3,4,5};
            var nClosest = 1;
            
            //Act and assert 
            try 
            { 
                var result = helper.FindClosestNumbers(needle, haystack, nClosest).ToList();
                Assert.Fail("no exception thrown");
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is OurTheSameNumberAsInHaystackException); //define this exception 
            }
        }
    }
}
