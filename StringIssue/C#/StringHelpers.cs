﻿using System.Collections.Generic;

namespace StringIssue
{
    public static class StringHelpers
	{
		/// <summary>
		/// Method that does not perform well.
		/// </summary>
		/// <param name="strs"></param>
		/// <returns></returns>
		public static string MergeStrings(IEnumerable<string> strs)
		{
			var toReturn = "";
			foreach (var str in strs)
			{
				toReturn += str;
			}
			return toReturn;
		}

        public static string MergeStrings2(IEnumerable<string> strs)
		{
				return string.Concat(strs);
		}
	}
}

/* Explain why your solution is faster below this line
 * 
    here I cannot explain deeply why this works better, but as a rule,
    if there is a method inside the .NET framework that does just what I want, 
    it is for sure optimized. I simply found this method by browsing available 
    methods for 'string' in documentation. If there is not such a method, 
    I would try to decrease iterations, reads etc., just like in the previous excercises 
*/
