﻿using System;

namespace VacationCost
{
    public class TransportMethod
    {
        public string NameOfMethod { get; set; }
        public int PriceMultiplier { get; set; }

        public TransportMethod(string nameOfMethod, int priceMultiplier)
        {
            NameOfMethod = nameOfMethod;
            PriceMultiplier = priceMultiplier;
        }
    }
}
