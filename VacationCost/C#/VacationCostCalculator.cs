﻿using System;
using System.Collections.Generic;

namespace VacationCost
{
    public class VacationCostCalculator
    {
        public List<TransportMethod> methods = new List<TransportMethod>();

        public void PopulateTheMethods() 
        {
            methods.Add(new TransportMethod("car",1));
            methods.Add(new TransportMethod("plane",2));
            methods.Add(new TransportMethod("ship",3));
        }

        public TransportMethod FindAndGetTransportMethod(string searchedMethod)
        {
            foreach ( TransportMethod method in methods)
                {
                    if (method.NameOfMethod == searchedMethod)
                    return method;
                }
            return null;
        }

        public int CostOfVacation(string transportMethod, string distance)
        {
            var distanceInt = Int32.Parse(distance);
            
            var method = FindAndGetTransportMethod(transportMethod);

            if (method != null)
            {
                var multiplier = method.PriceMultiplier;
                return distanceInt * multiplier;
            }
            else
            return 0;
        }
    }
}
