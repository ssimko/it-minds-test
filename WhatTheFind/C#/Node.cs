﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhatTheFind
{
    class Node
    {
        public int Value { get; set; }
        public IEnumerable<Node> Children { get; set; }
    }
}
