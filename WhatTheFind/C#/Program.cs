﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhatTheFind
{
    public class Program
    {
        static void Main(string[] args)
        {

            var nodeC = new Node { Value = 16 };
            var nodeD = new Node { Value = 28 };
            var nodeB = new Node { Value = 55, Children = new List<Node> { nodeC, nodeD } };
            var nodeA = new Node { Value = 26, Children = new List<Node> { nodeB } };
            

            var nodeX = nodeA.FindWhere(x => x.Value == 28, x => x.Children); // returns nodeB


            Console.WriteLine(nodeX.ToString() + " " + nodeX.Value.ToString());
            Console.ReadLine();
        }   
    }
}